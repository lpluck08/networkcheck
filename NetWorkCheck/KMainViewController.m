//
//  KMainViewController.m
//  NetWorkCheck
//
//  Created by corptest on 13-8-14.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import "KMainViewController.h"
#import "Reachability.h"

@interface KMainViewController ()

@property (strong, nonatomic) IBOutlet UILabel *hostLabel;

@property (strong, nonatomic) IBOutlet UILabel *internetLabel;

@property (strong, nonatomic) IBOutlet UILabel *wifiLabel;

@property (strong, nonatomic) IBOutlet UILabel *statusLabel;

@property (strong, nonatomic) Reachability *hostReach;

@property (strong, nonatomic) Reachability *internetReach;

@property (strong, nonatomic) Reachability *wifiReach;

@end

@implementation KMainViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    
    self.statusLabel.hidden = YES;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];
    
    // 检查网络请求是否可以到达指定的主机名
    self.hostReach = [Reachability reachabilityWithHostName:@"www.apple.com"];
    [self.hostReach startNotifier];
    [self refreshUIWithReachability:self.hostReach];
    
    // 检查路由连接是否有效
    self.internetReach = [Reachability reachabilityForInternetConnection];
    [self.internetReach startNotifier];
    [self refreshUIWithReachability:self.internetReach];
    
    // 检查wifi是否有效
    self.wifiReach = [Reachability reachabilityForLocalWiFi];
    [self.wifiReach startNotifier];
    [self refreshUIWithReachability:self.wifiReach];
}

- (void) viewDidUnload
{
    [self setHostLabel:nil];
    [self setInternetLabel:nil];
    [self setWifiLabel:nil];
    [self setStatusLabel:nil];
    
    [super viewDidUnload];
    
    [[NSNotificationCenter defaultCenter] removeObject:self];
}

- (void) didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - private

- (void) refreshLabel:(UILabel *)label reachability:(Reachability *)reach
{
    NSString *result = nil;
    label.textColor = [UIColor greenColor];
    switch ([reach currentReachabilityStatus]) {
        case NotReachable:
            label.textColor = [UIColor redColor];
            result = @"Failure  无网络可用";
            break;
        case ReachableViaWWAN:
            result = @"Success  2G/3G网络信号";
            break;
        case ReachableViaWiFi:
            result = @"Success  WIFI信号";
            break;
    }
    label.text = result;
}

- (void) refreshUIWithReachability:(Reachability *)reach
{
    if (reach == self.hostReach) {
        [self refreshLabel:self.hostLabel reachability:reach];
        
        self.statusLabel.hidden = [reach currentReachabilityStatus] != NotReachable;
    } else if (reach == self.internetReach) {
        [self refreshLabel:self.internetLabel reachability:reach];
    } else if (reach == self.wifiReach) {
        [self refreshLabel:self.wifiLabel reachability:reach];
    }
}

- (void) reachabilityChanged: (NSNotification* )note
{
	Reachability* reach = [note object];
	NSParameterAssert([reach isKindOfClass: [Reachability class]]);
	[self refreshUIWithReachability:reach];
}

#pragma mark - textfield delegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}


@end
