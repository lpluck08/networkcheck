//
//  KAppDelegate.h
//  NetWorkCheck
//
//  Created by corptest on 13-8-14.
//  Copyright (c) 2013年 klaus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface KAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
